//
// Created by joeverbist on 6/9/22.
//

#ifndef LINALG_LINALG_H
#define LINALG_LINALG_H


#include "matrix.h"
#include "matrix_constructors.h"
#include "vector_constructors.h"

// Doctest is just out of the source directory.
#include "../doctest.h"


TEST_CASE("Testing Gaussian Elimination - 1")
{
    Matrix A(3, 3, 2);
    A = A - Identity(3);
    linspace b(2);

    Matrix x = A/b;

    Matrix sol(3, 1);
    sol << 1.2,
           0.2,
          -0.8;

    CHECK(x.almost_equal(sol));

    Matrix c = A & x;
    CHECK(b.almost_equal(c, 1e-5));
}

TEST_CASE("Testing Gaussian Elimination - 2")
{
    Identity I(4);
    Matrix A = I + 2;
    linspace b(3);

    Matrix x = A/b;


    Matrix sol(4, 1);
    sol << (double) -4/3,
           (double) -1/3,
           (double)  2/3,
           (double)  5/3;

    CHECK(x.almost_equal(sol));
}

TEST_CASE("Testing Random Matrix solving - 3")
{
    RandomMatrix A(10, 0.0, 5.0);
    RandomMatrix b(10, 1, 0.0, 5.0);

    Matrix x = A/b;

    Matrix c = A & x;

    CHECK(b.almost_equal(c, 1e-12));
}

TEST_CASE("Testing Error throwing")
{
    bool exception_occured = false;
    try
    {
        Matrix A(3, 3, 2.0);
        Matrix b(3, 1, 0.0);

        Matrix x = A/b;
    }
    catch( Exception& e){
        exception_occured = true;
    }
    CHECK(exception_occured);
}

#endif //LINALG_LINALG_H