//
// Created by joeverbist on 6/9/22.
//
#include "vector_constructors.h"


int get_size(int min, int max, int step){
    return (max - min)/step + 1;  // This should floor properly...
}


double** linspace::create_linspace_array(int min, int max, int step) {
    int size = get_size(min, max, step);

    double ** arr;
    arr = new double*[4];

    for (int i=0; i<size; i++){
        arr[i] = new double[1];
        arr[i][0] = (double) min + i*step;
    }

    return arr;
}


linspace::linspace(int max) : Matrix(get_size(0, max, 1),
                                     1,
                                     create_linspace_array(0, max, 1)) {

}

linspace::linspace(int min, int max, int step) : Matrix(get_size(min, max, step),
                                                        1,
                                                        create_linspace_array(min, max, step)) {

}