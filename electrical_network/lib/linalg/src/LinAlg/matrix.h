#include <utility>
#include <iostream>

//
// Created by joeverbist on 6/9/22.
//

#ifndef LINALG_MATRIX_HPP
#define LINALG_MATRIX_HPP

class Matrix
{
private:
    // Related to pushing data into the array.
    int mInsertIndex = 0;
    void push_new_value(const double& a);
    void create_filled_matrix(int n_rows, int n_cols, double value);
    double** p_arr;
public:
    int matrix_number;

    // As opposed to the size, this will be updated when you transpose the matrix.
    int n_rows;
    int n_cols;

    // Constructors
    explicit Matrix(int size);
    Matrix(int rows, int columns);
    Matrix(int rows, int columns, double val);
    Matrix(int rows, int columns, double ** source_arr);

    // Copy overloading & assignement
    Matrix(const Matrix &obj);
    Matrix& operator=(const Matrix& m);

    // Examples of move constructor and assignment operator.
    Matrix(Matrix&& m ) noexcept ;
    Matrix& operator=(Matrix&& h) noexcept;

    // Destructor
    virtual ~ Matrix();

    // General helper functions
    void print();

    virtual void swap_rows(int row_num_a, int row_num_b);

    double& get(int row, int col);
    double read(int row, int col) const;

    virtual Matrix transpose();

    // These overloadings are all element-wise.
    Matrix operator + (const Matrix& m2);
    Matrix operator + (const int &val);
    Matrix operator + (const double &val);
    Matrix operator - (const Matrix &m2);
    Matrix operator - (const int &val);
    Matrix operator - (const double &val);
    Matrix operator * (const Matrix &m2);
    Matrix operator * (const int &val);
    Matrix operator * (const double &val);
    bool operator == (const Matrix& m2);
    bool operator != (const Matrix& m2);

    // Special accessing mechanism
    double& operator() (int i, int j);

    // Matrix multiplication
    Matrix operator & (const Matrix& m2) const;

    // Special inserting option.
    Matrix& operator << (const double& val);
    Matrix& operator , (const double& val);

    bool almost_equal(const Matrix& m2, const double& tol=1e-15);

    // The overloading of '/' is such that you can solve Ax=b by typing x = A/b.
    // Hence the '/' operator solve a linear system.
    Matrix operator / (const Matrix& m2) const;

    void drop_column(int col);
    void drop_row(int row);
    void add_row(int row);
    void add_column(int col);
};

void solve_upper_triangular(const Matrix& A, const Matrix& b, Matrix& x);

void ReduceToUpperTriangular(Matrix& m_A, Matrix& m_b);
Matrix solveGE(const Matrix& A, const Matrix& b);


class Exception
{
public:
    const char* _msg;

    explicit Exception(const char* msg)
    {
        _msg = msg;
    }
    void print() const
    {
        std::cout << _msg;
    }
};


#endif //LINALG_MATRIX_HPP
