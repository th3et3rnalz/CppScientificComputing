//
// Created by joeverbist on 6/9/22.
//

#ifndef LINALG_MATRIX_CONSTRUCTORS_HPP
#define LINALG_MATRIX_CONSTRUCTORS_HPP

class Identity: public Matrix{
private:
    static double** create_identity_array(int size);
public:
    explicit Identity(int size);
};

class Diagonal: public Matrix{
private:
    static double** create_diagonal_array(const double* vec, int size);
public:
    explicit Diagonal(double *vec, int size);
};


class RandomMatrix: public Matrix{
private:
    static double** create_random_array(int n_rows, int n_columns, double mean, double stddev);
public:
    explicit RandomMatrix(int size, double mean, double stddev);
    explicit RandomMatrix(int n_rows, int n_columns, double mean, double stddev);
};

#endif //LINALG_MATRIX_CONSTRUCTORS_HPP
