//
// Created by joeverbist on 6/9/22.
//

#include "matrix.h"
#include <iostream>
#include <cassert>
#include "../doctest.h"


// =====================================================================================================================
// =================================================   CONSTRUCTORS   ==================================================
// =====================================================================================================================

int GLOBAL_n_matrices = 0;
double GLOBAL_tol = 1e-15;

/*!
 * Helper function for constructing a matrix with a homogeneous value.
 *
 * @param rows: The number of rows
 * @param columns: The number of columns
 * @param value: The value that should be set everywhere in the matrix.
 */
void Matrix::create_filled_matrix(int rows, int columns, double value)
{
    n_rows = rows;
    n_cols = columns;

    matrix_number = GLOBAL_n_matrices;
    GLOBAL_n_matrices += 1;

    p_arr = new double* [rows];
    for (int i=0; i<n_rows; i++)
    {
        p_arr[i] = new double [columns];

        for (int j=0; j<n_cols; j++)
        {
            p_arr[i][j] = value;
        }
    }
}

/*!
 * Constructor for a square matrix with value 0 everywhere.
 *
 * @param size: The number of rows and columns
 */
Matrix::Matrix(int size)
{
    create_filled_matrix(size, size, 0);
}

/*!
 * Constructor for a rectangular matrix with value 0 everywhere
 *
 * @param rows: The number of rows
 * @param columns: The number of columns
 */
Matrix::Matrix(int rows, int columns)
{
    create_filled_matrix(rows, columns, (double) 0.0);
}

/*!
 * Constructor for a rectangular matrix with a homogenous value
 *
 * @param rows: The number of rows
 * @param columns: The number of columns
 * @param val: The value that should be set everywhere.
 */
Matrix::Matrix(int rows, int columns, double val)
{
    create_filled_matrix(rows, columns, val);

    // To ensure that you can't push new values into this array.
    // As it isn't intuitive how that should happen.
    mInsertIndex = rows*columns;
}

/*!
 * Constructor for a matrix from a source array
 *
 * @param rows: The number of rows
 * @param columns: The number of columns
 * @param source_arr: The source array. This must be a pointer to an array of pointers, who all point towards arrays.
 */
Matrix::Matrix(int rows, int columns, double **source_arr)
{
    n_rows = rows;
    n_cols = columns;

    // To ensure that you can't push new values into this array.
    // As it isn't intuitive how that should happen.
    mInsertIndex = rows*columns;

    matrix_number = GLOBAL_n_matrices;
    GLOBAL_n_matrices += 1;

    p_arr = source_arr;
}

// Pushing data into the matrix.
Matrix& Matrix::operator<<(const double &val)
{
    push_new_value(val);
    return *this;
}

Matrix& Matrix::operator,(const double &val)
{
    push_new_value(val);
    return *this;
}

void Matrix::push_new_value(const double &a)
{
    // First we must find the appropriate place to put the new value.
    int col = mInsertIndex % n_cols;
    int row = mInsertIndex / n_cols;

    assert (row < this->n_rows);

    (*this)(row, col) = a;
    mInsertIndex++;
}


// =====================================================================================================================
// ============================================   BASIC MATRIX FUNCTIONS   =============================================
// =====================================================================================================================

void Matrix::print()
{
    // Frequent use of the std namespace in this function, hence:
    using namespace std;

    cout << "Matrix_"<< matrix_number <<" (" << n_rows << "x" << n_cols << ")" << endl;
    for (int row=0; row < n_rows; row++)
    {
        cout << "| ";
        for (int col=0; col < n_cols; col++)
        {
            cout << (*this)(row, col) << " ";

            // To add clarity when the entries are floats (non-fixed length).
            if (col < n_cols-1)
            {
                cout << ". ";
            }
        }
        cout << "|" << endl;
    }
    cout << endl;
}

/*!
 * Function to access (both read and write) the data in the matrix. In conjunction with the read function,
 * these are the two functions that should be used to interact with the data. This is to allow easy changing of
 * the underlying data structure.
 * @param row: The row to be accessed
 * @param col: The column to be accessed
 * @return
 */
double& Matrix::get(int row, int col)
{
    if ((row < 0) or (row > n_rows) or (col < 0) or (col > n_cols))
    {
        std::cout << "Array indexing invalid. Requirements: \n";
        std::cout << "0 <= i <= " << n_rows-1 << " and 0 <= j <= " << n_cols-1 << "\n";
        std::cout << "But got i=" << row << " and j=" << col << "\n";

        throw;
    }

    return p_arr[row][col];
}

double Matrix::read(int row, int col) const
{
    return p_arr[row][col];
}

inline double& Matrix::operator()(int row, int col)
{
    return get(row, col);
}

/*!
 * Creates a transposed matrix of "this", leaving 'this' unmodified.
 * @return Transpose of "this".
 */
Matrix Matrix::transpose()
{
    int n_col_t = n_rows;
    int n_row_t = n_cols;

    auto** new_arr = new double* [n_row_t];
    for (int row=0; row<n_row_t; row++)
    {
        new_arr[row] = new double[n_col_t];
        for (int col=0; col<n_col_t; col++)
        {
            new_arr[row][col] = this->read(col, row);
        }
    }

    return {n_row_t, n_col_t, new_arr};
}

TEST_CASE("Testing Basic Matrix functions - 3")
{
    Matrix A(3, 3);

    // Testing input sequence.
    A << 1, 2, 3,
            4, 5, 6,
            7, 8, 9;

    // Testing assignment using the overloaded () operator and then accessing using read;
    A(2, 2) = 12;
    CHECK(A.read(2, 2) == 12);

    // Checking the transpose function.
    Matrix B = A.transpose();
    Matrix A_c = B.transpose();

    CHECK(A_c == A);
}

// =====================================================================================================================
// ==============================================   MEMORY MANAGEMENT   ================================================
// =====================================================================================================================

// Destructor
Matrix::~Matrix()
{
    // If p_arr = nullptr, that means it was probably moved and can't be deleted.
    if (p_arr != nullptr)
    {
        // We only have to take care of the pointer arrays, the rest is automatically deleted.
        for (int i = 0; i < n_rows; ++i) {
            delete[] p_arr[i];
        }
        delete[] p_arr;
    }
}

// Copy constructor
Matrix::Matrix(const Matrix &source)
{
    p_arr = new double* [source.n_rows];
    for (int row=0; row<source.n_rows; row++)
    {
        p_arr[row] = new double[source.n_cols];
        for (int col=0; col<source.n_cols; col++)
        {
            p_arr[row][col] = source.read(row, col);
        }
    }

    n_rows = source.n_rows;
    n_cols = source.n_cols;

    matrix_number = GLOBAL_n_matrices;
    GLOBAL_n_matrices++;
}

// Move constructor
Matrix::Matrix(Matrix &&m) noexcept{
    matrix_number = GLOBAL_n_matrices;
    GLOBAL_n_matrices += 1;

    this->mInsertIndex =m.mInsertIndex;
    this->p_arr = m.p_arr;
    this->n_rows = m.n_rows;
    this->n_cols = m.n_cols;

    m.p_arr = nullptr;
}

// Copy assignment operator
Matrix &Matrix::operator=(const Matrix &m)
{
    if (this == &m)
    {
        return *this;
    }

    Matrix tmp(m);

    // This is exception safe.
    // We can't use assignment, as then the destruction operator on tmp would deallocated "this".
    std::swap(p_arr, tmp.p_arr);

    return *this;
}

// Move assignment operator
Matrix &Matrix::operator=(Matrix &&h)  noexcept {
    // We must first delete this' array.
    if (p_arr != nullptr)
    {
        // We only have to take care of the pointer arrays, the rest is automatically deleted.
        for (int i = 0; i < n_rows; ++i) {
            delete[] p_arr[i];
        }
        delete[] p_arr;
    }

    this->mInsertIndex =h.mInsertIndex;
    this->p_arr = h.p_arr;
    this->n_rows = h.n_rows;
    this->n_cols = h.n_cols;

    h.p_arr = nullptr;
    return *this;
}

// =====================================================================================================================
// =============================================   OPERATOR OVERLOADING   ==============================================
// =====================================================================================================================

// Function type definitions so that the 'combine_two_matrices' and 'compare_two_matrices' can take functions.
typedef double f_double(const double& a, const double& b);
typedef bool f_bool(const double& a, const double& b);

/*!
 * This function combines two matrices using a given element-wise operation.
 * It requires both input matrices to have the same number of rows and columns, and returns a similarly sized Matrix.
 *
 * @param m1: First matrix.
 * @param m2: Second matrix
 * @param func: The elementwise operation used to combine the two matrices.
 * @return
 */
Matrix combine_two_matrices(const Matrix& m1, const Matrix& m2, f_double func)
{
    assert (m1.n_rows == m2.n_rows);
    assert (m1.n_cols == m2.n_cols);

    double** _new_arr;
    _new_arr = new double* [m1.n_rows];
    for (int i=0; i<m1.n_rows; i++)
    {
        _new_arr[i] = new double [m1.n_rows];

        for (int j=0; j<m1.n_cols; j++)
        {
            _new_arr[i][j] = func(m1.read(i, j), m2.read(i, j));
        }
    }
    return {m1.n_rows, m1.n_cols, _new_arr};
}

/*!
 * This function returns a new matrix that has been modified according to the
 * func and val given.
 *
 * @param m1: First matrix.
 * @param val: The single value for the operation.
 * @param func: The elementwise operation used to create the new matrix.
 * @return
 */
Matrix unit_matrix_transform(const Matrix& m1, const double& val, f_double func)
{
    double** _new_arr;
    _new_arr = new double* [m1.n_rows];
    for (int row=0; row<m1.n_rows; row++)
    {
        _new_arr[row] = new double [m1.n_cols];

        for (int col=0; col<m1.n_cols; col++)
        {
            _new_arr[row][col] = func(m1.read(row, col), val);
        }
    }
    return {m1.n_rows, m1.n_cols, _new_arr};
}

/*!
 * This function compares two matrices using a given element-wise operation.
 * It requires both input matrices to have the same number of rows and columns, and returns a boolean.
 * It has value true if all element-wise operations returned true, otherwise false.
 *
 * @param m1: First matrix.
 * @param m2: Second matrix
 * @param func: The elementwise operation used to compare the two matrices.
 * @return
 */
bool compare_two_matrices(const Matrix &m1, const Matrix &m2, f_bool func)
{
    assert (m1.n_rows == m2.n_rows);
    assert (m1.n_cols == m2.n_cols);

    bool _res = true;

    for (int i=0; i<m1.n_rows; i++)
    {
        for (int j=0; j<m1.n_cols; j++)
        {
            if (!func(m1.read(i, j ), m2.read(i, j)))
            {
                _res = false;
            }
        }
    }
    return _res;
}

inline double unit_add(const double &a, const double &b)
{
    return a+b;
}

inline double unit_subtract(const double &a, const double &b)
{
    return a-b;
}

inline double unit_multiply(const double &a, const double &b)
{
    return a*b;
}

inline bool unit_almost_equal(const double &a, const double &b)
{
    return std::abs(a - b) < GLOBAL_tol;
}

inline bool unit_equal(const double &a, const double &b)
{
    return a == b;
}

inline bool unit_not_equal(const double &a, const double &b)
{
    return a != b;
}

// -- ADDITION -- ADDITION -- ADDITION -- ADDITION -- ADDITION -- ADDITION -- ADDITION --
Matrix Matrix::operator+(const Matrix &m2)
{
    return combine_two_matrices(*this, m2, unit_add);
}

Matrix Matrix::operator+(const int &val)
{
    return unit_matrix_transform(*this, (double) val, unit_add);
}

Matrix Matrix::operator+(const double &val)
{
    return unit_matrix_transform(*this, val, unit_add);
}

TEST_CASE("Testing Matrix Addition")
{
    // Making the matrices non-square to find row v. col mix-ups.
    Matrix A(3, 2, 1);
    Matrix B(3, 2, 2);

    Matrix C = A+2;
    Matrix D = A+2.0;
    Matrix E = A + B;

    // First checking that they are all equal.
    CHECK(C == D);
    CHECK(C == E);

    // Then checking specific values.
    double** s;
    s = new double* [3];
    for (int row=0; row<3; row++)
    {
        s[row] = new double[2];
        s[row][0] = row*2;
        s[row][1] = row*2+1;
    }
    Matrix F(3, 2, s);

    Matrix G = F+A;

    // Manually checking that all the entries are correct.
    CHECK(G(0,0) == 1);
    CHECK(G(0, 1) == 2);
    CHECK(G(1, 0) == 3);
    CHECK(G(1, 1) == 4);
    CHECK(G(2, 0) == 5);
    CHECK(G(2, 1) == 6);
}

// -- SUBTRACTION -- SUBTRACTION -- SUBTRACTION -- SUBTRACTION -- SUBTRACTION -- SUBTRACTION --
Matrix Matrix::operator-(const Matrix &m2)
{
    return combine_two_matrices(*this, m2, unit_subtract);
}

Matrix Matrix::operator-(const int &val)
{
    return unit_matrix_transform(*this, (double) val, unit_subtract);
}

Matrix Matrix::operator-(const double &val)
{
    return unit_matrix_transform(*this, val, unit_subtract);
}

TEST_CASE("Testing Matrix Subtraction")
{
    // This test is a bit lazy: it just checks that subtraction is equivalent to addition with the opposite value.
    // It relies on the implementation of addition to be correct to imply subtraction is correct.
    // However, this isn't a problem because the only difference between those two operations is the
    // 'unit_operator' function (unit_add replaced by unit_subtract), so more in depth tests would simply test the same
    // code twice.

    int pos = 3;
    int neg = -pos;

    Matrix A(4, 1, 1);
    Matrix B_pos(4, 1, pos);
    Matrix B_neg(4, 1, neg);


    Matrix C = A + pos;
    Matrix D = A - neg;
    Matrix E = A - (double) neg;
    Matrix F = A + (double) pos;
    Matrix G = A + B_pos;
    Matrix H = A - B_neg;

    // Everything should be equal.
    CHECK(C == D);
    CHECK(C == E);
    CHECK(C == F);
    CHECK(C == G);
    CHECK(C == H);
}

// -- MULTIPLICATION -- MULTIPLICATION -- MULTIPLICATION -- MULTIPLICATION -- MULTIPLICATION --
Matrix Matrix::operator*(const Matrix &m2)
{
    return combine_two_matrices(*this, m2, unit_multiply);
}

Matrix Matrix::operator*(const int &val)
{
    return unit_matrix_transform(*this, (double) val, unit_multiply);
}

Matrix Matrix::operator*(const double &val)
{
    return unit_matrix_transform(*this, val, unit_multiply);
}

// -- COMPARISON -- COMPARISON -- COMPARISON -- COMPARISON -- COMPARISON -- COMPARISON --
bool Matrix::operator == (const Matrix& m2)
{
    return compare_two_matrices(*this, m2, unit_equal);
}

bool Matrix::operator != (const Matrix& m2)
{
    return compare_two_matrices(*this, m2, unit_not_equal);
}

bool Matrix::almost_equal(const Matrix& m2, const double& tol)
{
    GLOBAL_tol = tol;
    return compare_two_matrices(*this, m2, unit_almost_equal);
}


// =============== LINEAR SYSTEM SOLVING ===============

/*!
 * Using Gaussian elimination to solve Ax=b by writing A/b.
 *
 * @param m2: The column vector b. It needs to have the same number of rows as A.
 * @return A column vector with the same number of rows as A has rows.
 */
Matrix Matrix::operator/(const Matrix& m2) const
{
    return solveGE(*this, m2);
}

// THE UNIT TESTS FOR GAUSSIAN ELIMINATION ARE IN THE linalg.hpp FILE AS THE SETUP
// IS GREATLY SIMPLIFIED WHEN USING THE HELPER_SETUP CLASSES SUCH AS IDENTITY AND
// LINSPACE.

Matrix Matrix::operator&(const Matrix& m2) const
{
    assert (this->n_cols == m2.n_rows);

    auto** m = new double*[this->n_rows];
    for (int _row=0; _row < this->n_rows; _row++)
    {
        m[_row] = new double[m2.n_cols];
    }


    for (int _row=0; _row < this->n_rows; _row++)
    {

        for (int _col=0; _col < m2.n_cols; _col++)
        {
            m[_row][_col] = 0;

            for (int i=0; i<this->n_cols; i++)
            {
                m[_row][_col] += this->read(_row, i) * m2.read(i, _col);
            }
        }
    }
    Matrix x(this->n_rows, m2.n_cols, m);
    return x;
}

/*!
 * Swaps two rows in the Matrix.
 *
 * @param row_num_a: index of first row
 * @param row_num_b: index of second row
 */
void Matrix::swap_rows(int row_num_a, int row_num_b) {
    double* tmp = p_arr[row_num_a];
    p_arr[row_num_a] = p_arr[row_num_b];
    p_arr[row_num_b] = tmp;
}


/*!
 * Drops a columns from the matrix
 *
 * @param col: The index of the to-be-removed column
 */
void Matrix::drop_column(int col) {
    assert (col < n_cols);

    for (int _row=0; _row<n_rows; _row++)
    {
        // We first allocate a new row
        auto* p_row = new double[n_rows-1];
        for (int _col=0; _col<n_cols; _col++)
        {
            if (_col < col){
                p_row[_col] = p_arr[_row][_col];
            } else if (_col > col){
                p_row[_col-1] = p_arr[_row][_col];
            }
            // if they're equal, we skip and do nothing.
        }

        // Then we swap the rows.
        std::swap(p_row, p_arr[_row]);

        // and finally deallocate the old p_arr row.
        delete[] p_row;
    }
    n_cols--;
}

TEST_CASE("TEST drop column") {
    // The main place for issues to pop is with memory management.

    Matrix A(3);
    A << 1, 2, 3,
         4, 5, 7,
         8, 8, 10;

    A.drop_column(1);

    CHECK(A(0, 1) == 3);
    CHECK(A(1, 1) == 7);
    CHECK(A(2, 1) == 10);
    CHECK(A.n_cols == 2);
    CHECK(A.n_rows == 3);

    // Next test is to ensure that all data is properly allocated and readable.
    for (int _row=0; _row<A.n_rows; _row++)
    {
        for (int _col=0; _col<A.n_cols; _col++)
        {
            A.read(_row, _col);
        }
    }

    A.drop_column(0);
    CHECK(A(0, 0) == 3);
    CHECK(A(1, 0) == 7);
    CHECK(A(2, 0) == 10);

    A.drop_column(0);
    CHECK(A.n_cols == 0);
    CHECK(A.n_rows == 3);
}

/*!
 * Drops a row from the matrix
 *
 * @param row: The index of the row to be removed.
 */
void Matrix::drop_row(int row) {
    // First we create a new root array.
    auto** p_new_root = new double* [n_rows-1];

    for (int _row=0; _row < n_rows; _row++)
    {
        if (_row < row)
            p_new_root[_row] = p_arr[_row];
        else if (_row == row)
            delete[] p_arr[_row];
        else
            p_new_root[_row-1] = p_arr[_row];
    }
    std::swap(p_new_root, p_arr);
    delete[] p_new_root;
    n_rows--;
}

TEST_CASE("TEST drop row") {
    // The main place for issues to pop is with memory management.

    Matrix A(3);
    A << 1, 2, 3,
        4, 5, 7,
        8, 8, 10;

    A.drop_row(1);


    CHECK(A(1, 0) == 8);
    CHECK(A(1, 1) == 8);
    CHECK(A(1, 2) == 10);
    CHECK(A.n_cols == 3);
    CHECK(A.n_rows == 2);

    // Next test is to ensure that all data is properly allocated and readable.
    for (int _row=0; _row<A.n_rows; _row++)
    {
        for (int _col=0; _col<A.n_cols; _col++)
        {
            A.read(_row, _col);
        }
    }
}

/*!
 * Adds a row and fills it with zeros.
 * The rows with indices >'row' will be moved down.
 *
 * @param row: The index at which the row should be placed
 */
void Matrix::add_row(int row) {
    assert (row < n_rows+1);

    // --- CREATING THE NEW ROW ---
    auto* p_new_row = new double[n_cols];
    for (int _col=0; _col<n_cols; _col++)
    {
        p_new_row[_col] = 0.0;
    }

    // -- CREATING A NEW ROOT ARRAY --
    auto** p_new_root = new double*[n_rows+1];
    n_rows++;

    // -- MOVING OLD TO NEW ROOT ARRAY WITH SPACE AT row.
    for (int _row=0; _row < n_rows; _row++)
    {
        if (_row < row)
            p_new_root[_row] = p_arr[_row];
        else if (_row == row)
            p_new_root[row] = p_new_row;
        else
            // We are shuffling everything over by one spot.
            p_new_root[_row] = p_arr[_row-1];
    }

    // -- INSERTING NEW ROOT AND DELETING OLD --;
    std::swap(p_arr, p_new_root);
    delete[] p_new_root;
}

TEST_CASE("Test add row")
{
    Matrix A(2, 3);
    A << 1, 2, 3,
         4, 5, 6;

    A.add_row(2);

    CHECK(A(0, 1) == 2);
    CHECK(A(1, 1) == 5);
    CHECK(A(2, 0) == 0);
    CHECK(A(2, 1) == 0);
    CHECK(A(2, 2) == 0);
    CHECK(A.n_rows == 3);
    CHECK(A.n_cols == 3);

    // Next test is to ensure that all data is properly allocated and readable.
    for (int _row=0; _row<A.n_rows; _row++)
    {
        for (int _col=0; _col<A.n_cols; _col++)
        {
            A.read(_row, _col);
        }
    }


    // Let's try inserting and dropping various rows to get it to break.
    Matrix B(3);  // 3x3 matrix.
    B << 1, 2, 3; // Rest is automatically zero.

    B.add_row(1);
    B.add_row(4);
    B.add_row(0);
    B.drop_row(5);
    B.drop_row(0);
}


/*!
 * Adds column of zeros.
 *
 * @param col: index at which the new column should be added.
 */
void Matrix::add_column(int col) {
    // We need to replace each row with a new row with one extra entry.
    n_cols++;
    for (int _row=0; _row<n_rows; _row++)
    {
        double *p_row_new;
        p_row_new = new double[n_cols];

        for (int _col=0; _col<n_cols; _col++)
        {
            if (_col < col)
                p_row_new[_col] = this->read(_row, _col);
            else if (_col == col)
                p_row_new[_col] = 0.0;
            else
                p_row_new[_col] = this->read(_row, _col-1);
        }
        std::swap(p_row_new, p_arr[_row]);
        delete[] p_row_new;
    }
}

TEST_CASE("Test add column"){
    Matrix A(2, 3);
    A << 1, 2, 3,
         4, 5, 6;

    // Inserting at the beginning.
    A.add_column(0);

    // Inserting at the end.
    A.add_column(4);

    // Inserting in the middle
    A.add_column(3);

    CHECK(A.n_rows == 2);
    CHECK(A.n_cols == 6);

    CHECK(A(0, 1) == 1);
    CHECK(A(1, 1) == 4);

    CHECK(A(0, 4) == 3);
    CHECK(A(1, 4) == 6);

    // Next test is to ensure that all data is properly allocated and readable.
    for (int _row=0; _row<A.n_rows; _row++)
    {
        for (int _col=0; _col<A.n_cols; _col++)
        {
            A.read(_row, _col);
        }
    }

}

/*!
 * This function solves a linear system Ax=b for an upper triangular matrix A.
 *
 * @param m_A: The A matrix (Must be upper triangular, won't be modified)
 * @param m_b: The b matrix (won't be modified)
 * @param x: (a reference to ) the vector in which the result is to be stored.
 */
void solve_upper_triangular(const Matrix& m_A, const Matrix& m_b, Matrix& x)
{
    // As the matrix is upper triangular, we shall start iterating from the bottom.
    for (int row=m_A.n_rows-1; row>=0; row--)
    {
        // The equation we need to solve is:
        // a_{row, row} x_{row} + a_{row, row+1} x_{row+1} + .. a_{row, n} x_{n} = b_{row}
        // Hence we first assign x_{row} = b_{row}
        x(row, 0) = m_b.read(row, 0);

        // Then subtract all a_{row, j} x_{j} for j in (row+1, n)
        for (int col=row+1; col<m_A.n_rows; col++)
        {
            x(row, 0) -= m_A.read(row, col) * x(col, 0);
        }
        // And finally divide by a_{row, row}
        if (m_A.read(row, row) == 0){
            throw Exception( "Matrix is singular");
        }
        x(row, 0) = x(row, 0) / m_A.read(row, row);
    }
}

/*!
 * Solves, using Gaussian Elimination with pivoting,
 * the system Ax=b for x, and returns x.
 *
 * @param A: The Matrix
 * @param b: The Vector
 * @return Matrix x.
 */
Matrix solveGE(const Matrix &A, const Matrix &b)
{
    // Quickly checking that we have the right sizes.
    assert (A.n_cols == b.n_rows);
    assert (b.n_cols == 1);
    assert (A.n_rows == A.n_cols);

    // Given that we don't want to modify the original matrices, we need to make the 
    // modifications on copies of the matrices:
    Matrix m_A(A);
    Matrix m_b(b);
    
    // Then we reduce the system to upper triangular.
    ReduceToUpperTriangular(m_A, m_b);

    // Then we solve the upper triangular system.
    Matrix x(A.n_cols, 1);
    solve_upper_triangular(m_A, m_b, x);
    
    return x;
}

/*!
 * Reduces [A, b]  using row Elementary transformations to A upper triangular.
 * @param A
 * @param b
 */
void ReduceToUpperTriangular(Matrix& A, Matrix& b){
    // This function iterates over rows and columns (sub-diagonal entries only). For a given
    // (_row, _col), it performs the operation over the entire row, iterating over _icol.

    // holder variable for the row reduction;
    double _h_reductions;
    
    // holder variables for the partial pivoting. 
    int _h_index_max;
    double _h_value_max;
    
    // Don't need to perform any calculations on the final column, hence until A.n_cols-1
    for (int _col=0; _col<A.n_cols-1; _col++)
    {
        // ----------------------------------------
        // --------- PARTIAL PIVOTING -------------
        // ----------------------------------------
        _h_index_max = _col;
        _h_value_max = A.read(_col, _col);
        
        for (int _irow=_col+1; _irow<A.n_rows; _irow++){
            // We look for the index containing the largest value.
            if (A.read(_irow, _col) > _h_value_max)
            {
                _h_value_max = A.read(_irow, _col);
                _h_index_max = _irow;
            }
        }
        // Then we swap the rows;
        A.swap_rows(_col, _h_index_max);
        b.swap_rows(_col, _h_index_max);
        

        // ----------------------------------------
        // --------- ROW REDUCTION -------------
        // ----------------------------------------
        for (int _row=_col+1; _row<A.n_rows; _row++)
        {
            _h_reductions = A.read(_row, _col) / A.read(_col,_col);

            if (A.read(_row, _col) == 0){
                continue;
            }

            for (int _icol=_col; _icol<A.n_cols; _icol++)
            {
                A(_row, _icol) -= _h_reductions*A.read(_col, _icol);
            }
            b(_row, 0) -= _h_reductions* b(_col, 0);

        }
    }
}
