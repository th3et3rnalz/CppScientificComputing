//
// Created by joeverbist on 6/9/22.
//
#include "matrix.h"
#include "matrix_constructors.h"
#include <random>


// =====================================================================================================================
// =============================================   IDENTITY MATRIX   ===================================================
// =====================================================================================================================

Identity::Identity(int size) : Matrix(size, size, create_identity_array(size)) {
}

double **Identity::create_identity_array(int size) {
    double** arr;
    arr = new double* [size];
    for (int i=0; i< size; i++){
        arr[i] = new double[size];

        for (int j=0; j<size; j++){
            if (i==j){
                arr[i][j] = 1;
            } else {
                arr[i][j] = 0;
            }
        }
    }
    return arr;
}



// =====================================================================================================================
// =============================================   DIAGONAL MATRIX   ===================================================
// =====================================================================================================================

Diagonal::Diagonal(double *vec, int size) : Matrix(size, size, create_diagonal_array(vec, size)) {

}

double **Diagonal::create_diagonal_array(const double *vec, int size) {
    double** arr;
    arr = new double*[size];

    for (int i=0; i<size; i++){
        arr[i] = new double[size];
        for (int j=0; j<size; j++){
            if (i==j){
                arr[i][j] = vec[i];
            } else{
                arr[i][j] = 0.0;
            }
        }
    }
    return arr;
}

double **RandomMatrix::create_random_array(int n_rows, int n_columns, double mean, double stddev) {
    std::default_random_engine generator;
    std::normal_distribution<double> distribution(mean, stddev);

    double** arr;
    arr = new double*[n_rows];

    for (int i=0; i<n_rows; i++){
        arr[i] = new double[n_columns];
        for (int j=0; j<n_columns; j++){
            arr[i][j] = distribution(generator);
        }
    }
    return arr;
}

RandomMatrix::RandomMatrix(int size, double mean, double stddev): Matrix(size, size, create_random_array(size, size, mean, stddev)) {

}

RandomMatrix::RandomMatrix(int n_rows, int n_columns, double mean, double stddev): Matrix(n_rows, n_columns, create_random_array(n_rows, n_columns, mean, stddev)) {

}
