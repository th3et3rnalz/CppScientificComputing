//
// Created by joeverbist on 6/9/22.
//

#ifndef LINALG_VECTOR_CONSTRUCTORS_HPP
#define LINALG_VECTOR_CONSTRUCTORS_HPP

#include "matrix.h"

class linspace: public Matrix
{
private:
    static double** create_linspace_array(int min, int max, int step);
public:
    explicit linspace(int max);
    explicit linspace(int min, int max, int step=1);
};


#endif //LINALG_VECTOR_CONSTRUCTORS_HPP
