//
// Created by joeverbist on 6/10/22.
//
#include "iostream"
#include "lines.h"

Line::Line(Node *node_a, double line_impedance, double potential_source, Node *node_b) {
    this->b = potential_source;
    this->p_node_a = node_a;
    this->p_node_b = node_b;
    this->line_impedance = line_impedance;
}

void Line::print() {
    p_node_a->print();
    std::cout << " |-- R=" << line_impedance << "Ω, U=" << b << "V --| ";
    p_node_b->print();
    std::cout << std::endl;
}

