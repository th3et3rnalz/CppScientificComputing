//
// Created by joeverbist on 6/10/22.
//

#include "iostream"
#include "network.h"
#include <LinAlg/linalg.h>
#include <cassert>
#include <fstream>
#include <cstdlib>
#include "../doctest.h"

Network::Network(Line **line_arr, int n_lines,
                 int n_nodes, int reference_node): B(n_lines, n_nodes),
                                                   C(n_lines, n_lines),
                                                   p(n_lines, 1)
{
    this->line_arr = line_arr;
    this->n_lines = n_lines;
    this->n_nodes = n_nodes;

    starting_index_nodes = GLOBAL_n_nodes - n_nodes;

    assert (reference_node < n_nodes);
    assert (0 < reference_node);
    this->n_reference_node = reference_node;

    // -- Building C, B, and p --
    for (int _line=0; _line<n_lines; _line++)
    {
        C(_line, _line) = 1.0 / (line_arr[_line]->line_impedance);
        p(_line, 0) = line_arr[_line]->b;
        B(_line, line_arr[_line]->p_node_a->node_number -starting_index_nodes)= -1;
        // End station
        B(_line, line_arr[_line]->p_node_b->node_number -starting_index_nodes) = 1;
    }
}

void Network::print()
{
    std::cout << "======================" << std::endl;
    std::cout << "== Printing network ==" << std::endl;
    std::cout << "======================" << std::endl;

    for (int i=0; i<n_lines; i++)
    {
        line_arr[i]->print();
    }
}

void Network::solve(Matrix& x)
{
    assert (x.n_rows == n_nodes);
    assert (x.n_cols == 1);

    Matrix B_drop(B);
    B_drop.drop_column(n_reference_node);

    Matrix m_A = B_drop.transpose() & C & B_drop;
    Matrix m_b = B_drop.transpose() & C & p;

    x = solveGE(m_A, m_b);

    // The new value will be set to zero, no need to overwrite.
    x.add_row(n_reference_node);
}

void Network::visualize(std::string filepath) {
    std::ofstream file;
    file.open("network.dot");

    file << "digraph {\n";

    for (int _line=0; _line<n_lines; _line++){
        int asciiValue = 65;
        char n_node_a = char(asciiValue + line_arr[_line]->p_node_a->node_number - starting_index_nodes);
        char n_node_b = char(asciiValue + line_arr[_line]->p_node_b->node_number - starting_index_nodes);

        file << "  " << n_node_a << " -> " << n_node_b << " [label=\"R=" << line_arr[_line]->line_impedance << "Ω ";
        if (line_arr[_line]->b < 0)
            file << "U=" << line_arr[_line]->b << "V";
        else if (line_arr[_line]->b > 0)
            file << "U=+" <<line_arr[_line]->b << "V";
        file << "\"]\n";
    }
    file << "}\n";
    file.close();

    std::string str = "dot -Ktwopi -Tpng -o" + filepath + " network.dot";
    const char * c = str.c_str();
    system(c);
}

void Network::map(const Matrix &x, Matrix &y) {
    assert (y.n_rows == n_lines);
    assert (y.n_cols == 1);

    assert (x.n_rows == n_nodes);
    assert (x.n_cols == 1);

    y = C & (p - (B& x));
}

TEST_CASE("Test electrical network solving"){
    Node node_1;
    Node node_2;
    Node node_3;
    Node node_4;
    Node node_5;

    double u_1 = -2;
    double u_2 = 4;

    Line line_1(&node_1, 1, 0, &node_4);
    Line line_2(&node_2, 1, 0, &node_3);
    Line line_3(&node_3, 1, u_2, &node_4);
    Line line_4(&node_1, 1, u_1, &node_2);
    Line line_5(&node_4, 1, 0, &node_5);
    Line line_6(&node_5, 1, 0, &node_2);

    Line* line_arr[] = {&line_1, &line_2, &line_3, &line_4, &line_5, &line_6};

    Network main_network(line_arr, 6, 5, 4);

    Matrix x(5,1);
    main_network.solve(x);

    CHECK(x.n_rows == 5);
    CHECK(x(0, 0) == 1);
    CHECK(x(1, 0) == -1);
    CHECK(x(2, 0) == -2);
    CHECK(x(3, 0) == 1);
    CHECK(x(4, 0) == 0);\

    Matrix y(6, 1);

    main_network.map(x, y);

    CHECK(y(0, 0) == 0);
    CHECK(y(1, 0) == 1);
    CHECK(y(2, 0) == 1);
    CHECK(y(3, 0) == 0);
    CHECK(y(4, 0) == 1);
    CHECK(y(5, 0) == 1);
}

