//
// Created by joeverbist on 6/10/22.
//
#include <iostream>
#include "nodes.h"

int GLOBAL_n_nodes = 0;


Node::Node()
{
    node_number = GLOBAL_n_nodes;
    GLOBAL_n_nodes++;
}

void Node::print()
{
    std::cout << "Node " << node_number;
}