//
// Created by joeverbist on 6/10/22.
//

#ifndef LINALG_LINES_H
#define LINALG_LINES_H

#include "nodes.h"
#include <complex>

class Line{

public:
    Node* p_node_a;
    Node* p_node_b;
    double b;
    Line(Node *node_a, double line_impedance, double potential_source, Node *node_b);
    void print();

    double line_impedance;
};

#endif //LINALG_LINES_H
