//
// Created by joeverbist on 6/10/22.
//

#ifndef LINALG_NETWORK_H
#define LINALG_NETWORK_H

#include "lines.h"
#include <LinAlg/linalg.h>
#include "nodes.h"
#include "lines.h"

class Network
{
private:
    Line** line_arr;
    int n_lines;
    int n_nodes;
    int n_reference_node;
    int starting_index_nodes;

    Matrix B;
    Matrix C;
    Matrix p;
public:
    Network(Line* line_arr[], int n_lines, int n_nodes, int reference_node);
    void solve(Matrix& x);
    void print();
    void visualize(std::string filepath);
    void map(const Matrix& x, Matrix& y);
};

#endif //LINALG_NETWORK_H
