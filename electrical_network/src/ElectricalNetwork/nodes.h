//
// Created by joeverbist on 6/10/22.
//

#ifndef LINALG_NODES_H
#define LINALG_NODES_H

extern int GLOBAL_n_nodes;

class Node
{
public:
    int node_number;

    Node();
    void print();
};

#endif //LINALG_NODES_H
