# Code for the 'C++ for Scientific Computing Course'


This repository contains the code written as part of a special topic report for the 'C++ for
Scientific Computing' course taught as part of the Mathematical Modelling and Scientific
Computing MSc at the University of Oxford. This course teaches the basics of C++, and hence
this code is not expected to be useful in a development scenario, but could be an interesting
read for a beginner in C++.


This codebase consists of 2 libraries: a very simple linear algebra library and a power flow 
network library (which uses the linear algebra library). For unit testing, Doctest is used,
such that the tests are written right next to the code (as opposed to being in a separate 
folder) and runtime, just before the main code.

Main features:
* Matrix and Vector classes implementation with operator overloading
* Gaussian elimination for linear solving (not suitable for large systems)
* Power network solver (work in progress)

<br>

To Do:
* More features in the linear algebra library.
* Add suitable destructor for Matrix class.
* Implement an iterative solver for larger systems
* More unit tests and documentation
* Implement solving system for power network
* Visualisation of power network and possibly solution too
* Import power network from file(s)
* and possible more to come.
