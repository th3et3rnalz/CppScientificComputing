#include <complex>

#define DOCTEST_CONFIG_IMPLEMENT
#include <doctest.h>
#include <ElectricalNetwork/network.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
    doctest::Context ctx;
    ctx.setOption("abort-after", 5);  // default - stop after 5 failed asserts
    ctx.applyCommandLine(argc, argv); // apply command line - argc / argv
    ctx.setOption("no-breaks", true); // override - don't break in the debugger
    int res = ctx.run();              // run test cases unless with --no-run
    if(ctx.shouldExit())              // query flags (and --exit) rely on this
        return res;                   // propagate the result of the tests


    Node node_1;
    Node node_2;
    Node node_3;
    Node node_4;
    Node node_5;

    double u_1 = -2;
    double u_2 = 4;

    Line line_1(&node_1, 1, 0, &node_4);
    Line line_2(&node_1, 1, u_1, &node_2);
    Line line_3(&node_2, 1, 0, &node_3);
    Line line_4(&node_3, 1, u_2, &node_4);
    Line line_5(&node_4, 1, 0, &node_5);
    Line line_6(&node_5, 1, 0, &node_2);

    Line* line_arr[] = {&line_1, &line_2, &line_3, &line_4, &line_5, &line_6};

    Network main_network(line_arr, 6, 5, 4);

    Matrix x(5,1);
    main_network.solve(x);

    std::cout << "Electrical network solution:\n\n";
    x.print();

    main_network.visualize("network.png");

    return res;
}